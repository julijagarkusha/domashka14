import { Slider } from './modules/Slider.js';

const sliderElement = document.getElementById('slider');

const slides = [
    {
        img: './images/slide1.svg',
        text: 'Lorem Ipsum has been the industry\'s standard dummy text ever',
    },
    {
        img: './images/slide2.svg',
        text: 'It has survived not only five centuries',
    },
    {
        img: './images/slide3.svg',
        text: 'Lorem Ipsum is simply dummy text',
    },
    {
        img: './images/slide4.svg',
        text: 'Contrary to popular belief, Lorem Ipsum is not simply random text.',
    },
    {
        img: './images/slide5.svg',
        text: 'Lorem Ipsum comes from sections 1.10.32 and 1.10.33',
    },
    {
        img: './images/slide6.svg',
        text: 'It was popularised in the 1960s with the release',
    },
    {
        img: './images/slide7.svg',
        text: 'Lorem Ipsum is simply dummy text',
    },
    {
        img: './images/slide8.svg',
        text: 'De Finibus Bonorum et Malorum',
    },
    {
        img: './images/slide9.svg',
        text: 'Lorem Ipsum has been the industry\'s standard dummy text ever',
    },
    {
        img: './images/slide10.svg',
        text: 'Contrary to popular belief',
    },
]

const mySlider = new Slider(sliderElement, slides);

mySlider.init({
    autoplay: true,
    dots: true,
});
