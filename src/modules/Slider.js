export function Slider(sliderContainer, slides) {
  this.sliderContainer = sliderContainer;
  this.slides = slides;
  this.currentSlide = 0;
  this.intervalRef = null;

  this.goToSlide = function (slideNumber) {
    this.currentSlide = slideNumber;
    this.updateSlideContent();
    this.updateDots();
  }

  this.prevSlide = function () {
    if(this.currentSlide - 1 < 0) {
      this.goToSlide(this.slides.length - 1);
    } else {
      this.goToSlide(this.currentSlide - 1);
    }
  }

  this.nextSlide = function () {
    if(this.currentSlide + 2 > this.slides.length) {
      this.goToSlide(0);
    } else {
      this.goToSlide(this.currentSlide + 1);
    }
  }

  this.onArrowClickHandler = function (event) {
    const arrowType = event.target.getAttribute('aria-label');
    if(arrowType === 'prev') {
      this.prevSlide();
    } else {
      this.nextSlide();
    }
  }.bind(this)

  this.createElement = function (element, classList) {
    const domElement = document.createElement(element);
    domElement.classList.add(classList);

    return domElement;
  }

  this.updateSlideContent = function () {
    const slideImgElement = this.sliderContainer.querySelector('.slider__img');
    const slideTextElement = this.sliderContainer.querySelector('.slider__text');
    slideImgElement.setAttribute('src', slides[this.currentSlide].img);
    slideTextElement.innerText = slides[this.currentSlide].text;
  }

  this.updateDots = function () {
    const sliderDotElements = this.sliderContainer.querySelectorAll('.slider__dot');
    sliderDotElements.forEach((sliderDotElement, sliderDotIndex) => {
      sliderDotElement.classList.remove('slider__dot--active');
      sliderDotIndex === this.currentSlide && sliderDotElement.classList.add('slider__dot--active');

      sliderDotElement.addEventListener('click', () => {
        this.goToSlide(sliderDotIndex)
      });
    })
  }

  this.init = function (args = {}) {
    const {
      autoplay = false,
      duration = 3000,
      dots = false,
    } = args;

    const elements = [
      { tag: 'div', className: 'slider', parent: () => this.sliderContainer },
      { tag: 'div', className: 'slider__slides', parent: () => this.sliderContainer.querySelector('.slider') },
      { tag: 'div', className: 'slider__item', parent: () => this.sliderContainer.querySelector('.slider__slides') },
      { tag: 'h2', className: 'slider__text', parent: () => this.sliderContainer.querySelector('.slider__item') },
      { tag: 'img', className: 'slider__img', parent: () => this.sliderContainer.querySelector('.slider__item') },
      { tag: 'div', className: 'slider__arrows', parent: () => this.sliderContainer.querySelector('.slider') },
    ];

    const sliderArrowsArray = [
      {
        sliderArrow: {
          tag: 'button',
          className: 'slider__arrow',
        },
        sliderArrowImg: {
          tag: 'img',
          className: 'slider__icon--prev',
          src: './images/slider-arrow-prev.svg',
        },
        type: 'prev',
      },
      {
        sliderArrow: {
          tag: 'button',
          className: 'slider__arrow',
        },
        sliderArrowImg: {
          tag: 'img',
          className: 'slider__icon--next',
          src: './images/slider-arrow-next.svg',
        },
        type: 'next',
      },
    ]

    elements.forEach(item => {
      const element = this.createElement(item.tag, item.className);
      item.parent().appendChild(element);
    })

    sliderArrowsArray.forEach(item => {
      const sliderArrow = this.createElement(item.sliderArrow.tag, item.sliderArrow.className);
      sliderArrow.classList.add(`slider__arrow--${item.type}`);
      const sliderArrowImg = this.createElement(item.sliderArrowImg.tag, item.sliderArrowImg.className);
      sliderArrow.appendChild(sliderArrowImg);
      this.sliderContainer.querySelector('.slider__arrows').appendChild(sliderArrow);
      sliderArrow.setAttribute('aria-label', item.type);
      sliderArrowImg.setAttribute('src', item.sliderArrowImg.src);
      sliderArrow.addEventListener('click', this.onArrowClickHandler);
    })

    this.updateSlideContent();

    if (autoplay) {
      this.intervalRef = setInterval(() => {
        if (this.currentSlide + 2 > this.slides.length) {
          clearInterval(this.intervalRef);
          return;
        }
        this.nextSlide();
        dots && this.updateDots();
      }, duration)
    }

    if (dots) {
      const sliderDotsElement = this.createElement('div', 'slider__dots');
      const sliderDotElement = this.createElement('div', 'slider__dot');
      this.sliderContainer.querySelector('.slider').appendChild(sliderDotsElement);

      slides.forEach(() => {
        this.sliderContainer.querySelector('.slider__dots').appendChild(sliderDotElement.cloneNode());
      })

      this.updateDots();
    }
  }
}
